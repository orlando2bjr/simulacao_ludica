#include <iostream>
#include <fstream>
using std::cout;
using std::endl;
using std::ofstream;

#include "LCG.h"

int main()
{

	LCG generator;
	ofstream lcg{ "lcg.txt" };
	ofstream uniform{ "uniforme.txt" };
	ofstream exponential{ "exponencial.txt" };
	for (size_t i = 0; i < 100; i++)
	{
		lcg << generator.lcg() << endl;
	}
	lcg.close();

	uniform << "m�nimo, m�ximo, valores" << endl;
	for (size_t i = 0; i < 100; i++)
	{
		uniform << "0, 100, " << generator.uniform(0,100) << endl;
	}
	uniform.close();
	exponential << "m�dia, valores" << endl;
	for (size_t i = 0; i < 100; i++)
	{
		exponential << "50, " << generator.exponential(50) << endl;
	}
	exponential.close();
	ofstream normal("normal.txt");
	normal << "m�dia, desvio, valores" << endl;
	for (size_t i = 0; i < 100; i++)
	{
		normal << "8.2, 3.1, " << generator.normal(8.2, 3.1) << endl;
	}
	normal.close();
	ofstream normalErnesto("normalErnesto.txt");
	normalErnesto << "m�dia, desvio, valores" << endl;
	for (size_t i = 0; i < 100; i++)
	{
		normalErnesto << "8.2, 3.1, " << generator.normalErnesto(8.2, 3.1) << endl;
	}
	normalErnesto.close();


}

