#pragma once
#define _USE_MATH_DEFINES
#include <math.h>

enum class  SEED
{
	_32_BITS,
	TIME
};

class LCG
{
	unsigned int m_a;
	unsigned int m_c;
	unsigned int m_m;
	unsigned long long m_seed;
	unsigned int m_x;
public:

	LCG() 
		:m_a{ 1103515245 },
		m_c{ 12345 },
		m_m{ 4294967291 },
		m_seed{ 65537 },
		m_x{m_seed}
	{
//		m_x = m_seed;
	}

	~LCG()
	{
	}
	double lcg()
	{
		m_x = (m_a * m_x + m_c) % m_m;
		return double(m_x) / double(m_m - 1);
	}

	double uniform(double min, double max)
	{
		double delta = max - min;
		return min + (lcg() * delta);
	}

	double exponential(double mean)
	{
		return ((-mean) * log(1 - lcg()));
	}

	double normal(double mean = 0.0, double standardDeviation = 1.0)
	{
		return (mean + standardDeviation*cos(2.0 * M_PI * lcg())*sqrt(-log(lcg())));
	}
	double normalErnesto(double x_ = 0.0, double s = 1.0)
	{
		double w{ 0 }, v1{ 0 };
		do
		{
			double u1{ lcg() }, u2{ lcg() };
			v1 = 2 * u1 - 1;
			double v2 { 2 * u2 - 1 };
			w = pow(v1, 2) + pow(v2, 2);
		} while (w > 1);

		double y{ sqrt((-2.0 * log(w) / w)) };
		double x{ v1 * y };
		return (x_ + (x * s));
	}
};

